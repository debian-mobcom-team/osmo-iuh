Description: avoid implicit declaration of functions
--- a/tests/hnb-test-rua.c
+++ b/tests/hnb-test-rua.c
@@ -2,6 +2,7 @@
 #include <asn1c/ANY.h>
 #include <osmocom/ranap/ranap_common_cn.h>
 #include <osmocom/rua/rua_ies_defs.h>
+#include <osmocom/ranap/ranap_common_cn.h>
 
 #include "hnb-test-layers.h"
 
--- a/tests/test-hnbap.c
+++ b/tests/test-hnbap.c
@@ -20,6 +20,7 @@
  */
 
 #include <osmocom/ranap/iu_helpers.h>
+#include <osmocom/ranap/ranap_common_cn.h>
 #include "asn1helpers.h"
 
 #include <osmocom/hnbap/hnbap_common.h>
